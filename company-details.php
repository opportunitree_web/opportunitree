        <!-- page content -->
        <div class="" role="main">
          <div class="page-title">
            <div class="title_left">
              <h3 class="name"></h3>
            </div>
            <div class="title_right">
              <div class="col-xs-12 form-group text-right">
                <a class="btn btn-xs btn-success" href="add-company.php?id=<?=$_GET['id']?>"><i class="fa fa-edit"></i></a>
                <button class="btn btn-xs btn-danger" onclick=deleterecord('<?=$_GET[id]?>','companies','company.php')><i class="fa fa-trash-o"></i></button>
              </div>
            </div>
          </div>
          <div class="cleatfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Company Info</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li>
                      <a href="add-company.php?id=<?=$_GET['id']?>">
                        <i class="fa fa-pencil"></i>
                      </a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <p>
                    <b>Name:</b> <span class="name" ng-bind="companyName"></span>
                  </p>
                  <span>
                    <b>Company Logo:</b>
                    <span id="logoUrl"></span>
                    
                  </span>                  
                  <p>
                    <b>Size:</b> <span id="size" ng-bind="size"></span>
                  </p>
                  <p>
                    <b>Industry:</b> <span id="industry" ng-bind=""></span>
                  </p>
                  <p>
                    <b>Website:</b> <span id="website" ng-bind="website"></span>
                  </p>
                  <p>
                    <b>Address:</b> <span id="address" ng-bind="address"></span>
                  </p>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 col-sm-12">
                  <div class="x_panel">
                    <div class="x_title">
                      <h2>Jobs</h2>
                      <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li>
                          <a href="add-employer.php">
                            <i class="fa fa-plus"></i>
                          </a>
                        </li>
                      </ul>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                      <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                          <thead>
                            <tr class="headings">
                              <th class="column-title" style="display: table-cell;">Sl. No. </th>
                              <th class="column-title" style="display: table-cell;">Post</th>
                              <th class="column-title" style="display: table-cell;">Company</th>
                              <th class="column-title no-link last" style="display: table-cell;"><span class="nobr">Action</span>
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr class="even pointer">
                              <td class=" ">1</td>
                              <td>Java Developer</td>
                              <td>Barclays</td>
                              <td class=" last">
                                <button class="btn btn-xs btn-primary"><i class="fa fa-eye"></i></button>
                              </td>
                            </tr>
                            <tr class="odd pointer">
                              <td class=" ">2</td>
                              <td>Java Developer</td>
                              <td>Barclays</td>
                              <td class=" last">
                                <button class="btn btn-xs btn-primary"><i class="fa fa-eye"></i></button>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-sm-12">
                  <div class="x_panel">
                    <div class="x_title">
                      <h2>Members</h2>
                      <ul class="nav navbar-right panel_toolbox">
                        <li>
                          <div class="row">
                            <div class="col-md-8">
                              <div class="form-group">
                                <input type="text" class="form-control" placeholder="Enter Member ID...">
                              </div>
                            </div>
                            <div class="col-md-4">
                              <button class="btn btn-info">Add</button>
                            </div>
                          </div>
                        </li>
                      </ul>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                      <!-- com_member -->
                      <div ui-view="com_member"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->