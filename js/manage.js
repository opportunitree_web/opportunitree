var $output = $('#output');
var CURURL = 'http://' + $(location).attr('hostname') + '/OPPORTUNITREElatest/';//
var URL='http://admin-dev.api.opportunitree.ca/v1/';
$(function () {
   $('#add_employee').on('submit', function (e) {
  e.preventDefault()
  if($('#emp_enabled').is(":checked")){
    var enabled=true;
  }else{
    var enabled=false;
  }
  if($('#emp_activated').is(":checked")){
    var activated=true;
  }else{
    var activated=false;
  }
  if($('#id').val()!=''){
      var newuri="employers/"+$('#id').val();
      var method="PATCH";
    }else{
      var newuri="employers";
       var method="post";
    }
  var data=$('#add_employee').serialize()+'&'+$.param({ 'enabled': enabled })+'&'+$.param({ 'activated': activated });
  console.log(data);
  $.ajax({
    method: method,
    url: 'http://admin-dev.api.opportunitree.ca/v1/'+newuri,
    data: data,
    /*** This part is important ***/
    xhrFields: {
      withCredentials: true
    }
  }).then(function (response) {
    window.location.href=CURURL+"employers.php";
  }).catch(function (err) {
    alert('ERROR: ' + (err.responseText || 'Unspecified' ));
  })
})
})

function getemployers(){
  $.ajax({
      method: 'get',
      url: 'http://admin-dev.api.opportunitree.ca/v1/employers',
      /*** This part is important ***/
      xhrFields: {
        withCredentials: true
      }
    }).then(function (response) {
      //console.log(response);
      $.each(response.employers, function(index, element) {
        var record='';
        var record='<tr class="even pointer" onclick=window.location.href="employer-details.php?id='+element.id+'"><td>'+element.name+'</td><td>'+element.email+'</td>';
        if(element.profileCreated){
          record+='<td class="text-center"><i class="fa fa-check"></i></td>';
        }else{
           record+='<td class="text-center"><i class="fa fa-times"></i></td>';
        }
        if(element.enabled){
          record+='<td class="text-center"><i class="fa fa-check"></i></td>';
        }else{
           record+='<td class="text-center"><i class="fa fa-times"></i></td>';
        }
        if(element.activated){
          record+='<td class="text-center"><i class="fa fa-check"></i></td>';
        }else{
           record+='<td class="text-center"><i class="fa fa-times"></i></td>';
        }
        record+='<td class="text-center">'+formattedDate(element.lastActive)+'</td>';
        $(".append").append(record);
    });
    }).catch(function (err) {
      alert('ERROR: ' + (err.responseText || 'Unspecified' ));
    });
}
function getsingleEmployee(id,type){
  $.ajax({
      method: 'get',
      url: 'http://admin-dev.api.opportunitree.ca/v1/employers/'+id,
      /*** This part is important ***/
      xhrFields: {
        withCredentials: true
      }
    }).then(function (response) {
      if(type=='view'){
        $(".name").html(response.name);
        $("#email").html(response.email);
        $("#phone").html(response.phone);
        $("#accountType").html(response.accountType);
        //getempcompany(id);
          // getempinbox(id);
      }else{
       $("#emp_passwordForm").remove();// Remove the password field
       $("#emp_name").val(response.name); 
       $("#emp_email").val(response.email);
       $("#emp_profileCrt").val(response.profileCreated);
       $("#emp_plan").val(response.planID);
       $("#remobtn").html('');
       if(response.enabled){
          $("#emp_enabled").prop("checked",true);
        }
        if(response.activated){
          $("#emp_activated").prop("checked",true);
        }
      $("#remobtn").html('<button type="submit" class="btn btn-success">Update</button>');
    }
    }).catch(function (err) {
      alert('ERROR: ' + (err.responseText || 'Unspecified' ));
    });
}
