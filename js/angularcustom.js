var URL='http://admin-dev.api.opportunitree.ca/v1/';
var CURURL = 'http://' + $(location).attr('hostname') + '/OPPORTUNITREElatest/';//opportunitree
var oppertunitreeApp = angular.module('oppertunitreeApp', ['ui.router']);
oppertunitreeApp.config(function($stateProvider, $urlRouterProvider,$httpProvider) {
    $urlRouterProvider.otherwise('/home');
    $httpProvider.defaults.withCredentials = true;
    $stateProvider
        
        // HOME STATES AND NESTED VIEWS ========================================
        .state('home', {
            url: '/home',
            templateUrl: 'home.php'
        })
        // ABOUT PAGE AND MULTIPLE NAMED VIEWS =================================
        .state('jobseeker', {
            url: '/jobseeker',
            templateUrl: 'job-seeker.php',
            controller: 'jsController',
        })
        .state('addjobseeker', {
            url: '/addjobseeker',
            templateUrl: 'add-jobseeker.php',
            controller: 'addjsController',
        })
         .state('jsdetails', {
            url: '/jsdetails/:id',
            views: {
                '': { 
                  templateUrl: 'js-details.php',
                  controller: 'jsdetailController'
                 },
                'inboxview@jsdetails': { 
                  templateUrl: 'inbox-thread.php',
                  controller: 'inboxviewController'
                   },
                'resumeview@jsdetails': { 
                  templateUrl: 'resume.php',
                  controller: 'resumejsController'
                   },
                'jbappview@jsdetails': { 
                  templateUrl: 'job-application.php',
                  controller: 'jbappjsController'
                   }
            }
        })
         .state('empdetails', {
            url: '/empdetails/:id',
            views: {
                '': { 
                  templateUrl: 'employer-details.php',
                  controller: 'empdetailsController'
                 },
                'emp_inbox@empdetails': { 
                  templateUrl: 'inbox-thread.php',
                  controller: 'empinboxController'
                   },
                'emp_company@empdetails': { 
                  templateUrl: 'company.php',
                  controller: 'empcompanyController'
                   },
                   'emp_job@empdetails': { 
                  templateUrl: 'jobs.php',
                  controller: 'empjobController'
                   }
            }
        })
         .state('companyDetails', {
            url: '/companyDetails/:id',
            views: {
                '': { 
                  templateUrl: 'company-details.php',
                  controller: 'comdetailsController'
                 },
                'com_member@companyDetails': { 
                  templateUrl: 'com_member.php',
                  controller: 'commemberController'
                   }//,
                // 'emp_company@empdetails': { 
                //   templateUrl: 'emp_company.php',
                //   controller: 'empcompanyController'
                //    }
            }
        })
         .state('editjobseeker', {
            url: '/editjobseeker/:id',
            templateUrl: 'add-jobseeker.php',
            controller: 'editjsController',
        })
        .state('resume', {
            url: '/resume',
            templateUrl: 'resume.php',
            controller: 'rsController',
        })
        .state('rsdetails', {
            url: '/rsdetails/:id',
            templateUrl: 'resume-details.php',
            controller: 'rsdetailController'
        })
        .state('editresume', {
            url: '/editresume/:id',
            templateUrl: 'add-resume.php',
            controller: 'editrsController',
        })
        .state('employers', {
            url: '/employers',
            templateUrl: 'employers.php',
            controller: 'empController',
        })
        .state('addemployer', {
            url: '/addemployer',
            templateUrl: 'add-employer.php',
            controller: 'addempController',
        })
        .state('addresume', {
            url: '/addresume',
            templateUrl: 'add-resume.php',
            controller: 'addresController',
        })
        .state('company', {
            url: '/company',
            templateUrl: 'company.php',
            controller: 'cmpController',
        })
        .state('addcompany',{
           url: '/addcompany',
            templateUrl: 'add-company.php',
            controller: 'addcmpController',
        })
        .state('jobs',{
           url: '/jobs',
            templateUrl: 'jobs.php',
            controller: 'jobsController',
        })
        .state('addjob',{
           url: '/addjob',
            templateUrl: 'add-job.php',
            controller: 'addjobsController',
        })
        .state('jobdetails',{
           url: '/jobdetails/:id',
            templateUrl: 'job-details.php',
            controller: 'jobdetailsController',
        })
        .state('editjob',{
           url: '/editjob/:id',
            templateUrl: 'add-job.php',
            controller: 'editjobController'
        })
        .state('jobapplication',{
           url: '/jobapplication',
            templateUrl: 'job-application.php',
            controller:'jobappController'
        })
        .state('addjobapplication',{
           url: '/addjobapplication',
            templateUrl: 'add-jobapplication.php',
            controller: 'addjobappController'
        })
        .state('inbox',{
           url: '/inbox',
            templateUrl: 'inbox.php'
            //controller: 'inboxController'
        })
        .state('addnewmsg',{
           url: '/addnewmsg',
            templateUrl: 'composenew.php'
        })
        .state('inboxdetails',{
           url: '/inboxdetails',
            templateUrl: 'inboxdetails.php'
        });
        
});
/*
 Jobseeker listing
 */
oppertunitreeApp.controller('jsController', function($scope,$http) {
        $http.get(URL+"job-seekers", {
            params:{
            }
        }).success(function (response) {
         $scope.users = response.jobSeekers;
        }).error(function (err) {
        alert('ERROR: ' + (err.responseText || 'Unspecified' ));
      })
});
/*
 Add Jobseeker 
 */
oppertunitreeApp.controller('addjsController', function($scope,$http) {
  $scope.addjobseeker = function(){
        var enableddata=$('#enabled').is(":checked");
        var activateddata=$('#activated').is(":checked");
        var data =$("#jobseekerform").serialize()+'&'+$.param({ 'enabled': enableddata })+'&'+$.param({ 'activated':activateddata });;
        $http({
          method: 'POST',
          url: URL+"job-seekers",
          data: data,
         headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(response){
           alert("successfully added");
           $scope.jobseekerform.$setPristine();
           $scope.js = {};
        }).error(function (err) {
        alert('ERROR: ' + (err.responseText || 'Unspecified' ));
      })
      } 
});
/*
View Jobseeker details
 */
oppertunitreeApp.controller('jsdetailController', ['$scope', '$stateParams', '$http','$location',
    function($scope, $stateParams,$http,$location) {
      $http.get(URL+"job-seekers/"+$stateParams.id, {
            params:{
            }
        }).success(function (response) {
          $scope.id=response.id;
          $scope.name=response.name;
          $scope.email=response.email;
          $scope.phone=response.phone;
          $scope.profilecreate=response.profilecreate;
          $scope.enabled=response.enabled;
          $scope.activated=response.activated;
        }).error(function (err) {
        alert('ERROR: ' + (err.responseText || 'Unspecified' ));
      })
      $scope.deleterecord=function(id,page){
          if(confirm("Are you sure to delete?")){
             $http({
          method: 'DELETE',
          url: URL+page+"/"+id
        }).success(function (response) {
          alert("Record Deleted successfully");
          $location.path("/jobseeker");
         }).error(function (err) {
            alert('ERROR: ' + (err.responseText || 'Unspecified' ));
           })
          }
        }
  }])
oppertunitreeApp.controller('inboxviewController', ['$scope', '$stateParams', '$http',
    function($scope, $stateParams,$http) {
       $http.get(URL+"job-seekers/"+$stateParams.id+"/inbox", {
            params:{
            }
        }).success(function (response) {
          $scope.users=response.inbox;
        }).error(function (err) {
        alert('ERROR: ' + (err.responseText || 'Unspecified' ));
      })
  }])
oppertunitreeApp.controller('resumejsController', ['$scope', '$stateParams', '$http', 
    function($scope, $stateParams,$http) {
       $http.get(URL+"job-seekers/"+$stateParams.id+"/resumes", {
            params:{
            }
        }).success(function (response) {
          $scope.users=response.resumes;
        }).error(function (err) {
        alert('ERROR: ' + (err.responseText || 'Unspecified' ));
      })
  }])
oppertunitreeApp.controller('jbappjsController', ['$scope', '$stateParams', '$http',
    function($scope, $stateParams,$http) {
      $http.get(URL+"job-seekers/"+$stateParams.id+"/job-applications", {
            params:{
            }
        }).success(function (response) {
          $scope.users=response.jobapplications;
        }).error(function (err) {
        alert('ERROR: ' + (err.responseText || 'Unspecified' ));
      })
  }])
/* Edit Job seeker details*/
oppertunitreeApp.controller('editjsController', ['$scope', '$stateParams', '$http',
    function($scope, $stateParams,$http) {
      $scope.addjobseeker = function(){
        var enableddata=$('#enabled').is(":checked");
        var activateddata=$('#activated').is(":checked");
        var data =$("#jobseekerform").serialize()+'&'+$.param({ 'enabled': enableddata })+'&'+$.param({ 'activated':activateddata });;
        $http({
          method: 'PATCH',
          url: URL+"job-seekers/"+$stateParams.id,
          data: data,
         headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(response){
           alert("successfully Updated");
        }).error(function (err) {
        alert('ERROR: ' + (err.responseText || 'Unspecified' ));
      })
      } 
      $http.get(URL+"job-seekers/"+$stateParams.id, {
            params:{
            }
        }).success(function (response) {
          $("#rmpwd").html(" ");
         $scope.js =response;
        }).error(function (err) {
        alert('ERROR: ' + (err.responseText || 'Unspecified' ));
      })
  }])
/*
 Employeers listing
 */
oppertunitreeApp.controller('empController', function($scope,$http) {
        $http.get(URL+"employers", {
            params:{
            }
        }).success(function (response) {
         $scope.users = response.employers;
        }).error(function (err) {
        alert('ERROR: ' + (err.responseText || 'Unspecified' ));
      })
});
/*
 Add Employee 
 */
oppertunitreeApp.controller('addempController', function($scope,$http) {
  $scope.addemployee = function(){
    var enableddata=$('#emp_enabled').is(":checked");
    var activateddata=$('#emp_activated').is(":checked");
    var data =$("#add_employee").serialize()+'&'+$.param({ 'enabled': enableddata })+'&'+$.param({ 'activated':activateddata });;
    $http({
      method: 'POST',
      url: URL+"employers",
      data: data,
     headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).success(function(response){
       alert("successfully added");
       $scope.add_employee.$setPristine();
       $scope.emp = {};
    }).error(function (err) {
      alert('ERROR: ' + (err.responseText || 'Unspecified' ));
    })
  } 
});
/*
 Employee Details
 */
oppertunitreeApp.controller('empdetailsController', ['$scope', '$stateParams', '$http',
    function($scope, $stateParams,$http) {
      $http.get(URL+"employers/"+$stateParams.id, {
            params:{
            }
        }).success(function (response) {
          $scope.id=response.id;
          $scope.name=response.name;
          $scope.email=response.email;
          $scope.profilecreate=response.profilecreate;
          $scope.enabled=response.enabled;
          $scope.activated=response.activated;
          $scope.planId=response.planID;
          $scope.accountType=response.accountType;
        }).error(function (err) {
        alert('ERROR: ' + (err.responseText || 'Unspecified' ));
      })
  }])
oppertunitreeApp.controller('empcompanyController', ['$scope', '$stateParams', '$http',
    function($scope, $stateParams,$http) {
       $http.get(URL+"employers/"+$stateParams.id+"/companies", {
            params:{
            }
        }).success(function (response) {
          $scope.empCompanies=response.inbox;
        }).error(function (err) {
        alert('ERROR: ' + (err.responseText || 'Unspecified' ));
      })
  }])
/*
 Resume listing
 */
oppertunitreeApp.controller('rsController', function($scope,$http) {
        $http.get(URL+"resumes", {
            params:{
            }
        }).success(function (response) {
         $scope.users = response.resumes;
        }).error(function (err) {
        alert('ERROR: ' + (err.responseText || 'Unspecified' ));
      })
});

/*
 Resume details
 */
oppertunitreeApp.controller('rsdetailController', ['$scope', '$stateParams', '$http','$location',
    function($scope, $stateParams,$http,$location) {
      $http.get(URL+"resumes/"+$stateParams.id, {
            params:{
            }
        }).success(function (response) {
          $scope.id=response.id;
          $scope.name=response.firstName+" "+response.lastName;
          $scope.photourl=response.photoURL;
          $scope.pdfurl=response.pdfURL;
          $scope.address=response.address;
          $scope.qualification=response.qualifications;
          $scope.hoursPerWeek=response.hoursPerWeek;
          $scope.themeid=response.themeID;
          $scope.startWorking=response.startWorking;
        }).error(function (err) {
        alert('ERROR: ' + (err.responseText || 'Unspecified' ));
      })
      $scope.deleterecord=function(id){
          if(confirm("Are you sure to delete?")){
             $http({
          method: 'DELETE',
          url: URL+"resumes/"+id
        }).success(function (response) {
          alert("Record Deleted successfully");
          $location.path("/resume");
         }).error(function (err) {
            alert('ERROR: ' + (err.responseText || 'Unspecified' ));
           })
          }
        }
  }])
/*
 Add resume 
 */
oppertunitreeApp.controller('addresController', function($scope,$http) {

  $scope.addresume = function(){
        var isPDFOnly=$('#isPDFOnly').is(":checked");
    var public=$('#public').is(":checked");
    var morning=$('#wp1').is(":checked");
    var afternoon=$('#wp2').is(":checked");
    var evening=$('#wp3').is(":checked");
    var overnight=$('#wp4').is(":checked");
    var weekdays=$('#wp5').is(":checked");
    var weekends=$('#wp6').is(":checked");
    var data=$('#add_resume').serialize()+'&'+$.param({ 'isPDFOnly': isPDFOnly })+'&'+$.param({ 'public':public })+'&'+$.param({ 'workPreference[morning]': morning })+'&'+$.param({ 'workPreference[afternoon]':afternoon })+'&'+$.param({ 'workPreference[evening]': evening })+'&'+$.param({ 'workPreference[overnight]':overnight })+'&'+$.param({ 'workPreference[weekdays]': weekdays })+'&'+$.param({ 'workPreference[weekends]':weekends });
        $http({
          method: 'POST',
          url: URL+"resumes",
          data: data,
         headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(response){
           alert("successfully added");
           $scope.jobseekerform.$setPristine();
           $scope.rs = {};
        }).error(function (err) {
        alert('ERROR: ' + (err.responseText || 'Unspecified' ));
      })
      } 
});
/* Edit Resume details*/
oppertunitreeApp.controller('editrsController', ['$scope', '$stateParams', '$http',
    function($scope, $stateParams,$http) {
      $scope.addresume = function(){
        var public=$('#public').is(":checked");
    var morning=$('#wp1').is(":checked");
    var afternoon=$('#wp2').is(":checked");
    var evening=$('#wp3').is(":checked");
    var overnight=$('#wp4').is(":checked");
    var weekdays=$('#wp5').is(":checked");
    var weekends=$('#wp6').is(":checked");
    var data=$('#add_resume').serialize()+'&'+$.param({ 'isPDFOnly': isPDFOnly })+'&'+$.param({ 'public':public })+'&'+$.param({ 'workPreference[morning]': morning })+'&'+$.param({ 'workPreference[afternoon]':afternoon })+'&'+$.param({ 'workPreference[evening]': evening })+'&'+$.param({ 'workPreference[overnight]':overnight })+'&'+$.param({ 'workPreference[weekdays]': weekdays })+'&'+$.param({ 'workPreference[weekends]':weekends });
        $http({
          method: 'PATCH',
          url: URL+"resumes/"+$stateParams.id,
          data: data,
         headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(response){
           alert("successfully Updated");
        }).error(function (err) {
        alert('ERROR: ' + (err.responseText || 'Unspecified' ));
      })
      } 
      $http.get(URL+"resumes/"+$stateParams.id, {
            params:{
            }
        }).success(function (response) {
         $scope.rs = response;
        }).error(function (err) {
        alert('ERROR: ' + (err.responseText || 'Unspecified' ));
      })
  }])
/*
 Company listing
 */
oppertunitreeApp.controller('cmpController', function($scope,$http) {
        $http.get(URL+"companies", {
            params:{
            }
        }).success(function (response) {
         $scope.users = response.companies;
        }).error(function (err) {
        alert('ERROR: ' + (err.responseText || 'Unspecified' ));
      })
});
/*
 Add Company 
 */
oppertunitreeApp.controller('addcmpController', function($scope,$http) {
  $scope.addcompany = function(){
    var verified=$('#verified').is(":checked");
    var data=$('#add_company').serialize()+'&'+$.param({ 'verified': verified })
        $http({
          method: 'POST',
          url: URL+"companies",
          data: data,
         headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(response){
           alert("successfully added");
           $scope.addcompany.$setPristine();
           $scope.cmp = {};
        }).error(function (err) {
        alert('ERROR: ' + (err.responseText || 'Unspecified' ));
      })
      } 
});
oppertunitreeApp.controller('comdetailsController', ['$scope', '$stateParams', '$http',
    function($scope, $stateParams,$http) {
      $http.get(URL+"companies/"+$stateParams.id, {
            params:{
            }
        }).success(function (response) {console.log(response);
          $scope.id=response.id;
          $scope.companyName=response.companyName;
          $scope.address=response.address;
          $scope.size=response.size;
          $scope.website=response.website;
          $scope.activated=response.activated;
          $scope.verified=response.verified;
          $scope.logoURL=response.logoURL;
        }).error(function (err) {
        alert('ERROR: ' + (err.responseText || 'Unspecified' ));
      })
  }])
oppertunitreeApp.controller('commemberController', ['$scope', '$stateParams', '$http',
    function($scope, $stateParams,$http) {
       $http.get(URL+"companies/"+$stateParams.id+"/members", {
            params:{
            }
        }).success(function (response) {
          $scope.comMembers=response.inbox;
        }).error(function (err) {
        alert('ERROR: ' + (err.responseText || 'Unspecified' ));
      })
  }])
/*
 Jobs listing
 */
oppertunitreeApp.controller('jobsController', function($scope,$http) {
        $http.get(URL+"jobs", {
            params:{
            }
        }).success(function (response) {
         $scope.users = response.jobs;
        }).error(function (err) {
        alert('ERROR: ' + (err.responseText || 'Unspecified' ));
      })
});
/*
 Add Job 
 */
oppertunitreeApp.controller('addjobsController', function($scope,$http) {
  $scope.addjob = function(){
      var applyOnOT=$('#applyOnOT').is(":checked");
      var featured=$('#featured').is(":checked");
      var open=$('#open').is(":checked");
    var data=$('#add_job').serialize()+'&'+$.param({ 'applyOnOT': applyOnOT })+'&'+$.param({ 'featured':featured })+'&'+$.param({ 'open':open });
        $http({
          method: 'POST',
          url: URL+"jobs",
          data: data,
         headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(response){
           alert("successfully added");
           $scope.addformjob.$setPristine();
           $scope.job = {};
        }).error(function (err) {
        alert('ERROR: ' + (err.responseText || 'Unspecified' ));
      })
      } 
});
/*Job details & delete record*/
oppertunitreeApp.controller('jobdetailsController', ['$scope', '$stateParams', '$http','$location',
    function($scope, $stateParams,$http,$location) {
      $http.get(URL+"jobs/"+$stateParams.id, {
            params:{
            }
        }).success(function (response) {
          $scope.id=response.id;
          $scope.companyName=response.companyName;
          $scope.title=response.title;
          $scope.companyLogoURL=response.companyLogoURL;
          $scope.address=response.address;
          $scope.applyOnOT=response.applyOnOT;
          $scope.featured=response.featured;
          $scope.open=response.open;
          $scope.description=response.description;
        }).error(function (err) {
        alert('ERROR: ' + (err.responseText || 'Unspecified' ));
      })
      $scope.deleterecord=function(id){
          if(confirm("Are you sure to delete?")){
             $http({
          method: 'DELETE',
          url: URL+"jobs/"+id
        }).success(function (response) {
          alert("Record Deleted successfully");
          $location.path("/jobs");
         }).error(function (err) {
            alert('ERROR: ' + (err.responseText || 'Unspecified' ));
           })
          }
        }
  }])
/* Edit Job details*/
oppertunitreeApp.controller('editjobController', ['$scope', '$stateParams', '$http',
    function($scope, $stateParams,$http) {
      $scope.addjob = function(){
      var applyOnOT=$('#applyOnOT').is(":checked");
      var featured=$('#featured').is(":checked");
      var open=$('#open').is(":checked");
      var data=$('#add_job').serialize()+'&'+$.param({ 'applyOnOT': applyOnOT })+'&'+$.param({ 'featured':featured })+'&'+$.param({ 'open':open });
        $http({
          method: 'PATCH',
          url: URL+"jobs/"+$stateParams.id,
          data: data,
         headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(response){
           alert("successfully Updated");
        }).error(function (err) {
        alert('ERROR: ' + (err.responseText || 'Unspecified' ));
      })
      } 
      $http.get(URL+"jobs/"+$stateParams.id, {
            params:{
            }
        }).success(function (response) {
          if(response.applyOnOT){
            $("#applyExternURL").removeAttr("required");
          }
         $scope.job =response;
        }).error(function (err) {
        alert('ERROR: ' + (err.responseText || 'Unspecified' ));
      })
  }])
/*
 Jobapplication listing
 */
oppertunitreeApp.controller('jobappController', function($scope,$http) {
        $http.get(URL+"job-applications", {
            params:{
            }
        }).success(function (response) {
         $scope.users = response.jobApplications;
        }).error(function (err) {
        alert('ERROR: ' + (err.responseText || 'Unspecified' ));
      })
});
/*
Add Job application
 */
oppertunitreeApp.controller('addjobappController', function($scope,$http) {
  $scope.addapplicaion = function(){
      var refListAccess=$('#refListAccess').is(":checked");
    var data=$('#add_jobapplication').serialize()+'&'+$.param({ 'refListAccess': refListAccess });
        $http({
          method: 'POST',
          url: URL+"job-applications",
          data: data,
         headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(response){
           alert("successfully added");
           $scope.jobapplication.$setPristine();
           $scope.japp = {};
        }).error(function (err) {
        alert('ERROR: ' + (err.responseText || 'Unspecified' ));
      })
      } 
});
