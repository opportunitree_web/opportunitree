        <!-- page content -->
        <div class="" role="main">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>All Companies List</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li>
                      <a ui-sref="addcompany">
                        <i class="fa fa-plus"></i>
                      </a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <div class="table-responsive">
                    <table class="table table-striped jambo_table bulk_action">
                      <thead>
                        <tr class="headings">
                          <th class="column-title" style="display: table-cell;">Company Name</th>
                          <th class="column-title" style="display: table-cell;">Website</th>
                          <th class="column-title" style="display: table-cell;">Location</th>                          
                          <th class="column-title" style="display: table-cell;">Verified </th>
                          <th class="column-title" style="display: table-cell;">Size </th>
                          <th class="column-title" style="display: table-cell;">Industry </th>
                          </th>
                        </tr>
                      </thead>
                       <tbody>
                    <tr ng-repeat="user in users" ui-sref="companyDetails({id:user.id})">
                    <td>{{user.companyName}}</td>
                    <td>{{user.website}}</td>
                    <td>{{user.address}}</td>
                    <td>{{user.verified}}</td>
                    <td>{{user.size}}</td>
                    <td>{{user.industry}}</td>
                    </tr>
                    </tbody>
                    </table>
                  </div>
                </div>
               <!--  <div class="pull-right">
                  <ul class="pagination">
                  <li class="paginate_button previous disabled" id="datatable-keytable_previous">
                    <a href="#" aria-controls="datatable-keytable" data-dt-idx="0" tabindex="0"><i class="fa fa-chevron-left"></i></a>
                  </li>
                  <li class="paginate_button active">
                    <a href="#" aria-controls="datatable-keytable" data-dt-idx="1" tabindex="0">1</a>
                  </li>
                  <li class="paginate_button ">
                    <a href="#" aria-controls="datatable-keytable" data-dt-idx="2" tabindex="0">2</a>
                  </li>
                  <li class="paginate_button ">
                    <a href="#" aria-controls="datatable-keytable" data-dt-idx="3" tabindex="0">3</a>
                  </li>
                  <li class="paginate_button ">
                    <a href="#" aria-controls="datatable-keytable" data-dt-idx="4" tabindex="0">4</a>
                  </li>
                  <li class="paginate_button ">
                    <a href="#" aria-controls="datatable-keytable" data-dt-idx="5" tabindex="0">5</a>
                  </li>
                  <li class="paginate_button ">
                    <a href="#" aria-controls="datatable-keytable" data-dt-idx="6" tabindex="0">6</a>
                  </li>
                  <li class="paginate_button next" id="datatable-keytable_next">
                    <a href="#" aria-controls="datatable-keytable" data-dt-idx="7" tabindex="0"><i class="fa fa-chevron-right"></i></a>
                  </li>
                </ul>
                </div> -->
              </div>
            </div>
        </div>
        <!-- /page content -->
