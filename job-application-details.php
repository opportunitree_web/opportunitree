<!DOCTYPE html>
<html lang="en">
 <?php include_once 'common/head.php';?>
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
       <?php include_once 'common/menu.php';?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="page-title">
            <div class="title_left">
              <h3>Java Developer</h3>
            </div>
            <div class="title_right">
              <div class="col-xs-12 form-group text-right">
                <button class="btn btn-xs btn-success"><i class="fa fa-edit"></i></button>
                <button class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></button>
              </div>
            </div>
          </div>
          <div class="cleatfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Job Info</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li>
                      <a href="add-job.php">
                        <i class="fa fa-pencil"></i>
                      </a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <p>
                    <b>Company Name:</b> Barclays Co. Pvt Ltd.
                  </p>
                  <span>
                    <b>Company Logo:</b>
                    <img src="images/logo.jpg" alt="Logo" style="height: 75px;">
                  </span>                  
                  <p>
                    <b>Email:</b> barclays@gmail.com
                  </p>
                  <p>
                    <b>Website:</b> <a href="">www.barclays.com</a>
                  </p>
                  <p>
                    <b>Address:</b> Lane 8/23, San Fransisco, USA
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
      </div>
    </div>
    <?php include_once 'common/footer.php';?>
  </body>
</html>