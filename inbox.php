        <!-- page content -->
        <div class="" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Inbox <small>Some examples to get you started</small></h3>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Inbox<small>User Mail</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li>
                        <button class="btn btn-info btn-sm" ui-sref="addnewmsg">Compose New</button>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                    <div class="x_content">
                      <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                          <thead>
                            <tr class="headings">
                              <th class="column-title" style="display: table-cell;">Job Seeker ID</th>
                              <th class="column-title" style="display: table-cell;">Company ID</th>
                              <th class="column-title" style="display: table-cell;">Job Application ID</th> 
                              <th class="column-title" style="display: table-cell;">Subject</th>
                            </tr>
                          </thead>
                          <tbody>
                        <tr ui-sref="inboxdetails">
                          <td>OPTR2017xs001</td>
                          <td>ZYAG99rt12</td>
                          <td>OPJB2017dsf0001</td>
                          <td>Lorem ipsum dolor sit amet.</td>
                        </tr>
                        <tr>
                          <td>OPTR2017xs002</td>
                          <td>ZYAG99rt13</td>
                          <td>OPJB2017dsf0003</td>
                          <td>Lorem ipsum dolor sit amet.</td>
                        </tr>
                        <tr>
                          <td>OPTR2017xs003</td>
                          <td>ZYAG99rt14</td>
                          <td>OPJB2017dsf0003</td>
                          <td>Lorem ipsum dolor sit amet.</td>
                        </tr>
                        <tr>
                          <td>OPTR2017xs004</td>
                          <td>ZYAG99rt15</td>
                          <td>OPJB2017dsf0004</td>
                          <td>Lorem ipsum dolor sit amet.</td>
                        </tr>
                        <tr>
                          <td>OPTR2017xs005</td>
                          <td>ZYAG99rt16</td>
                          <td>OPJB2017dsf0005</td>
                          <td>Lorem ipsum dolor sit amet.</td>
                        </tr>
                        </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
     