<script type="text/javascript" charset="utf-8" async defer>
var $output = $('#output');
var CURURL = 'http://' + $(location).attr('hostname') + '/OPPORTUNITREElatest/';
var URL='http://admin-dev.api.opportunitree.ca/v1/';
$(function () {
 

  $('#login_form').on('submit', function (e) {
    e.preventDefault()
    $.ajax({
      method: 'post',
      url: 'http://admin-dev.api.opportunitree.ca/v1/login',
      data: {
        username: $('#login-username').val(),
        password: $('#login-password').val()
      },
      /*** This part is important ***/
      xhrFields: {
        withCredentials: true
      }
    }).then(function (response) {
       $.post(CURURL+"session.php",{},function(res){
            window.location.href=CURURL;
        });
    }).catch(function (err) {
      alert('Incorrect Login')
    })
  })
  $('#jobseekerform').on('submit', function (e) {
    e.preventDefault()
    if($('#enabled').is(":checked")){
      var enableddata=true;
    }else{
      var enableddata=false;
    }
    if($('#activated').is(":checked")){
      var activateddata=true;
    }else{
      var activateddata=false;
    }
    if($('#id').val()!=''){
      var newuri="job-seekers/"+$('#id').val();
      var method="PATCH";
    }else{
      var newuri="job-seekers";
       var method="post";
    }
    var data=$('#jobseekerform').serialize()+'&'+$.param({ 'enabled': enableddata })+'&'+$.param({ 'activated':activateddata });
    $.ajax({
      type: method,
      url: 'http://admin-dev.api.opportunitree.ca/v1/'+newuri,
      data: data,
      /*** This part is important ***/
      xhrFields: {
        withCredentials: true
      }
    }).then(function (response) {
      window.location.href=CURURL+"job-seeker.php";
    }).catch(function (err) {
      alert('ERROR: ' + (err.responseText || 'Unspecified' ));
    })
  })
   $('#add_company').on('submit', function (e) {
    e.preventDefault()
    if($('#verified').is(":checked")){
      var verified=true;
    }else{
      var verified=false;
    }
    if($('#id').val()!=''){
      var newuri="companies/"+$('#id').val();
      var method="PATCH";
    }else{
      var newuri="companies";
       var method="post";
    }
    var data=$('#add_company').serialize()+'&'+$.param({ 'verified': verified })+'&'+$.param({ 'website':$("#website").val() });
    $.ajax({
      type: method,
      url: 'http://admin-dev.api.opportunitree.ca/v1/'+newuri,
      data: data,
      /*** This part is important ***/
      xhrFields: {
        withCredentials: true
      }
    }).then(function (response) {
      window.location.href=CURURL+"company.php";
    }).catch(function (err) {
      alert('ERROR: ' + (err.responseText || 'Unspecified' ));
    })
  })
   $('#add_resume').on('submit', function (e) {
    e.preventDefault()
    if($('#isPDFOnly').is(":checked")){
      var isPDFOnly=true;
    }else{
      var isPDFOnly=false;
    }
    if($('#public').is(":checked")){
      var public=true;
    }else{
      var public=false;
    }
    if($('#wp1').is(":checked")){
      var morning=true;
    }else{
      var morning=false;
    }
    if($('#wp2').is(":checked")){
      var afternoon=true;
    }else{
      var afternoon=false;
    }
    if($('#wp3').is(":checked")){
      var evening=true;
    }else{
      var evening=false;
    }
    if($('#wp4').is(":checked")){
      var overnight=true;
    }else{
      var overnight=false;
    }
    if($('#wp5').is(":checked")){
      var weekdays=true;
    }else{
      var weekdays=false;
    }
    if($('#wp6').is(":checked")){
      var weekends=true;
    }else{
      var weekends=false;
    }
    if($('#id').val()!=''){
      var newuri="resumes/"+$('#id').val();
      var method="PATCH";
    }else{
      var newuri="resumes";
       var method="post";
    }
    var data=$('#add_resume').serialize()+'&'+$.param({ 'isPDFOnly': isPDFOnly })+'&'+$.param({ 'public':public })+'&'+$.param({ 'workPreference[morning]': morning })+'&'+$.param({ 'workPreference[afternoon]':afternoon })+'&'+$.param({ 'workPreference[evening]': evening })+'&'+$.param({ 'workPreference[overnight]':overnight })+'&'+$.param({ 'workPreference[weekdays]': weekdays })+'&'+$.param({ 'workPreference[weekends]':weekends });
    $.ajax({
      type:method,
      url: 'http://admin-dev.api.opportunitree.ca/v1/'+newuri,
      data: data,
      /*** This part is important ***/
      xhrFields: {
        withCredentials: true
      }
    }).then(function (response) {
            window.location.href=CURURL+"resume.php";
    }).catch(function (err) {
      alert('ERROR: ' + (err.responseText || 'Unspecified' ));
    })
  })
   $('#add_job').on('submit', function (e) {
    e.preventDefault()
    if($('#applyOnOT').is(":checked")){
      var applyOnOT=true;
    }else{
      var applyOnOT=false;
    }
    if($('#featured').is(":checked")){
      var featured=true;
    }else{
      var featured=false;
    }
    if($('#open').is(":checked")){
      var open=true;
    }else{
      var open=false;
    }
    if($('#id').val()!=''){
      var newuri="jobs/"+$('#id').val();
      var method="PATCH";
    }else{
      var newuri="jobs";
       var method="post";
    }
    var data=$('#add_job').serialize()+'&'+$.param({ 'applyOnOT': applyOnOT })+'&'+$.param({ 'featured':featured })+'&'+$.param({ 'open':open });
    $.ajax({
      type: method,
      url: 'http://admin-dev.api.opportunitree.ca/v1/'+newuri,
      data: data,
      /*** This part is important ***/
      xhrFields: {
        withCredentials: true
      }
    }).then(function (response) {
      window.location.href=CURURL+"jobs.php";
    }).catch(function (err) {
      alert('ERROR: ' + (err.responseText || 'Unspecified' ));
    })
  })
   $('#add_jobapplication').on('submit', function (e) {
    e.preventDefault()
    if($('#refListAccess').is(":checked")){
      var refListAccess=true;
    }else{
      var refListAccess=false;
    }
    if($('#id').val()!=''){
      var newuri="job-applications/"+$('#id').val();
      var method="PATCH";
    }else{
      var newuri="job-applications";
       var method="post";
    }
    var data=$('#add_jobapplication').serialize()+'&'+$.param({ 'refListAccess': refListAccess });
    $.ajax({
      type: method,
      url: 'http://admin-dev.api.opportunitree.ca/v1/'+newuri,
      data: data,
      /*** This part is important ***/
      xhrFields: {
        withCredentials: true
      }
    }).then(function (response) {
      window.location.href=CURURL+"job-application.php";
    }).catch(function (err) {
      alert('ERROR: ' + (err.responseText || 'Unspecified' ));
    })
  })
   $('.dateall').daterangepicker({
          singleDatePicker: true,
          calender_style: "picker_4"
        }, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
        });
})
function addexternalurl(){
  if($('#applyOnOT').is(":checked")){
      $("#applyExternURL").attr("required","required");
    }else{
      $("#applyExternURL").removeAttr("required");
    }
}
function getjobseeker(){
   $.ajax({
      method: 'get',
      url: 'http://admin-dev.api.opportunitree.ca/v1/job-seekers',
      /*** This part is important ***/
      xhrFields: {
        withCredentials: true
      }
    }).then(function (response) {
      $.each(response.jobSeekers, function(index, element) {
        var record='';
        var record='<tr class="even pointer" onclick=window.location.href="js-details.php?id='+element.id+'"><td>'+element.name+'</td><td>'+element.email+'</td>';
        if(element.enabled){
          record+='<td class="text-center"><i class="fa fa-check"></i></td>';
        }else{
           record+='<td class="text-center"><i class="fa fa-times"></i></td>';
        }
        if(element.activated){
          record+='<td class="text-center"><i class="fa fa-check"></i></td>';
        }else{
           record+='<td class="text-center"><i class="fa fa-times"></i></td>';
        }
        if(element.profileCreated){
          record+='<td class="text-center"><i class="fa fa-check"></i></td>';
        }else{
           record+='<td class="text-center"><i class="fa fa-times"></i></td>';
        }
        record+='<td class="text-center">'+formattedDate(element.lastActive)+'</td></tr>';
        $(".append").append(record);
    });
    }).catch(function (err) {
      alert('ERROR: ' + (err.responseText || 'Unspecified' ));
    });
}

function getsinglejobseeker(id,type){
   $.ajax({
      method: 'get',
      url: 'http://admin-dev.api.opportunitree.ca/v1/job-seekers/'+id,
      /*** This part is important ***/
      xhrFields: {
        withCredentials: true
      }
    }).then(function (response) {
      if(type=='view'){
        $(".name").html(response.name); 
          $("#email").html(response.email);
          $("#phone").html(response.phone);
          if(response.profileCreated){
          var record='<i class="fa fa-check"></i>';
          }else{
           var record='<i class="fa fa-times"></i>';
           }
           if(response.enabled){
          var enabled='<i class="fa fa-check"></i>';
          }else{
           var enabled='<i class="fa fa-times"></i>';
           }
           if(response.activated){
          var activated='<i class="fa fa-check"></i>';
          }else{
           var activated='<i class="fa fa-times"></i>';
           }
          $("#profilecreate").html(record);
          $("#enabled").html(enabled);
          $("#activated").html(activated);
          //getjobseekerresume(id);
          // getjobseekerapplication(id);
          // getjobseekerinbox(id);
      }else{
          $("#name").val(response.name); 
          $("#email").val(response.email);
          $("#phone").html(response.phone);
          $("#profileCreate").html(response.profileCreated);
          $("#rmpwd").remove();
          $("#remobtn").html('');
          $("#remobtn").html('<button type="submit" class="btn btn-success">Update</button>');
          if(response.enabled){
                $("#enabled").prop("checked",true);
            }
          if(response.activated){
                $("#activated").prop("checked",true);
            }
        }
    }).catch(function (err) {
      alert('ERROR: ' + (err.responseText || 'Unspecified' ));
    });
}

function getjobseekerresume(id){
  alert(id);
}
function deleterecord(id,method,page){
  if(confirm('Are you sure to delete')){
  $.ajax({
      url: 'http://admin-dev.api.opportunitree.ca/v1/'+method+'/'+id,
       type: 'DELETE',
      /*** This part is important ***/
      xhrFields: {
        withCredentials: true
      }
    }).then(function (response) {
        window.location.href=CURURL+page;        
    }).catch(function (err) {
      alert('ERROR: ' + (err.responseText || 'Unspecified' ));
    });
  }
}
function logout(){
  $.ajax({
      method: 'get', 
      url: URL + '/logout',
      /*** This part is important ***/
      xhrFields: {
        withCredentials: true
      }
    }).then(function () {
          $.post(CURURL+"unsetsession.php",{},function(res){
            window.location.href=CURURL;
        });   
    }).catch(function (err) {
      console.error(err)
      $output.text('ERROR: ' + (err.responseText || 'Unspecified'))            
    })
}
function getresume(){
  $.ajax({
      method: 'get',
      url: 'http://admin-dev.api.opportunitree.ca/v1/resumes',
      /*** This part is important ***/
      xhrFields: {
        withCredentials: true
      }
    }).then(function (response) {
      $.each(response.resumes, function(index, element) {
        var record='';
        var record='<tr class="even pointer" onclick=window.location.href="resume-details.php?id='+element.id+'"><td>'+element.firstName+' '+element.lastName+'</td><td>'+element.themeID+'</td>';
        if(element.public){
          record+='<td class="text-center"><i class="fa fa-check"></i></td>';
        }else{
           record+='<td class="text-center"><i class="fa fa-times"></i></td>';
        }
        if(element.isPDFOnly){
          record+='<td class="text-center"><i class="fa fa-check"></i></td>';
        }else{
           record+='<td class="text-center"><i class="fa fa-times"></i></td>';
        }
        record+='<td class="last">'+element.views+'</td></tr>';
        $(".append").append(record);
    });
    }).catch(function (err) {
      alert('ERROR: ' + (err.responseText || 'Unspecified' ));
    });
}
function getsingleresume(id,type){
  $.ajax({
      method: 'get',
      url: 'http://admin-dev.api.opportunitree.ca/v1/resumes/'+id,
      /*** This part is important ***/
      xhrFields: {
        withCredentials: true
      }
    }).then(function (response) {
      //alert('http://admin-dev.api.opportunitree.ca/v1/resumes/'+id);
      if(type=='view'){
        $("#name").html(response.firstName+" "+response.lastName);
        $("#address").html(response.address);
        $("#themeid").html(response.themeID);
        $("#hoursPerWeek").html(response.hoursPerWeek);
         $("#startWorking").html(formattedDate(response.startWorking));
      }else{
       $("#jobSeekerID").val(response.jobSeekerID); 
       $("#firstName").val(response.firstName);
       $("#lastName").val(response.lastName);
       $("#address").val(response.address);
       var statworing=formattedDate(response.startWorking);
       $("#startWorking").val(statworing);
       $("#themeID").val(response.themeID);
       $("#hoursPerWeek").val(response.hoursPerWeek);
       if(response.isPDFOnly){
          $("#isPDFOnly").prop("checked",true);
          $("#pdfhide").css("display","block");
          $("#pdfURL").val(response.pdfURL);
      }
      if(response.public){
          $("#public").prop("checked",true);
      }
      if(response.workPreference.morning){
          $("#wp1").prop("checked",true);
      }
      if(response.workPreference.evening){
          $("#wp2").prop("checked",true);
      }
      if(response.workPreference.weekdays){
          $("#wp3").prop("checked",true);
      }
      if(response.workPreference.weekends){
          $("#wp4").prop("checked",true);
      }
      if(response.workPreference.overnight){
          $("#wp5").prop("checked",true);
      }
      if(response.workPreference.afternoon){
          $("#wp6").prop("checked",true);
      }
      $("#remobtn").html('<button type="submit" class="btn btn-success">Update</button>');
    }
    }).catch(function (err) {
      alert('ERROR: ' + (err.responseText || 'Unspecified' ));
    });
}
function checkpdf(){
  if($("#isPDFOnly").is(":checked")){
    $("#pdfhide").css("display","block");
    $("#public").prop("checked",false);
  }else{
    $("#pdfhide").css("display","none");
  }
}
function checkpub(){
  if($("#isPDFOnly").is(":checked")){
    $("#public").prop("checked",false);
  }
}
function formattedDate(date) {
    var d = new Date(date || Date.now()),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [day, month, year].join('/');
}
function getcompany(){
  $.ajax({
      method: 'get',
      url: 'http://admin-dev.api.opportunitree.ca/v1/companies',
      /*** This part is important ***/
      xhrFields: {
        withCredentials: true
      }
    }).then(function (response) {
      $.each(response.companies, function(index, element) {
        var record='';
        var record='<tr class="even pointer" onclick=window.location.href="company-details.php?id='+element.id+'"><td>'+element.companyName+'</td><td>'+element.website+'</td><td>'+element.address+'</td>';
        if(element.verified){
          record+='<td class="text-center"><i class="fa fa-check"></i></td>';
        }else{
           record+='<td class="text-center"><i class="fa fa-times"></i></td>';
        }
        record+='<td>'+element.size+'</td><td>'+element.industry+'</td></tr>';
        $(".append").append(record);
    });
    }).catch(function (err) {
      alert('ERROR: ' + (err.responseText || 'Unspecified' ));
    });
}
function getsinglecompany(id,type){
  $.ajax({
      method: 'get',
      url: 'http://admin-dev.api.opportunitree.ca/v1/companies/'+id,
      /*** This part is important ***/
      xhrFields: {
        withCredentials: true
      }
    }).then(function (response) {
      if(type=='view'){
        $(".name").html(response.companyName);
        $("#logoUrl").html('<img src="'+response.logoURL+'" alt="Logo" style="height: 75px;">');
        $("#website").html('<a href="'+response.website+'">'+response.website+'</a>');
        $("#address").html(response.address);
        $("#size").html(response.size);
        $("#industry").html(response.industry);
        //getcompanyjob(id);
        //getcompanymember(id);
      }else{
       $("#companyName").val(response.companyName); 
       $("#Phone").val(response.Phone);
       $("#website").val(response.website);
       $("#address").val(response.address);
       $("#location").val(response.location);
       $("#logoURL").val(response.logoURL);
       $("#size").val(response.size);
       $("#industry").val(response.industry);
       $("#remobtn").html('');
       if(response.verified){
          $("#verified").prop("checked",true);
      }
      $("#remobtn").html('<button type="submit" class="btn btn-success">Update</button>');
    }
    }).catch(function (err) {
      alert('ERROR: ' + (err.responseText || 'Unspecified' ));
    });
}
function getjob(){
  $.ajax({
      method: 'get',
      url: 'http://admin-dev.api.opportunitree.ca/v1/jobs',
      /*** This part is important ***/
      xhrFields: {
        withCredentials: true
      }
    }).then(function (response) {
      $.each(response.companies, function(index, element) {
        var record='';
        var record='<tr class="even pointer"><td>'+element.id+'</td><td>'+element.companyName+'</td><td>'+element.website+'</td><td>'+element.address+'</td>';
        if(element.verified){
          record+='<td class="text-center"><i class="fa fa-check"></i></td>';
        }else{
           record+='<td class="text-center"><i class="fa fa-times"></i></td>';
        }
        record+='<td class="last"><a class="btn btn-xs btn-primary" href="company-details.php?id='+element.id+'"><i class="fa fa-eye"></i></a><a class="btn btn-xs btn-success" href="add-company.php?id='+element.id+'"><i class="fa fa-edit"></i></a><a class="btn btn-xs btn-danger" onclick=deleterecord("'+element.id+'","companies","company.php")><i class="fa fa-trash-o"></i></a></td></tr>';
        $(".append").append(record);
    });
    }).catch(function (err) {
      alert('ERROR: ' + (err.responseText || 'Unspecified' ));
    });
}
function getconstats(){
    $.ajax({
      method: 'get',
      url: 'http://admin-dev.api.opportunitree.ca/v1/constants',
      /*** This part is important ***/
      xhrFields: {
        withCredentials: true
      }
    }).then(function (response) {
      //alert(JSON.stringify(response, null, 2));
      $.post(CURURL+"constants.php",{"response":response},function(res){
        });
    }).catch(function (err) {
      alert('ERROR: ' + (err.responseText || 'Unspecified' ));
    });
}
	
</script>