 <!-- jQuery -->
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <!-- Angular JS -->
    <script src="http://code.angularjs.org/1.2.13/angular.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/angular-ui-router/0.2.8/angular-ui-router.min.js"></script>
    <!-- Bootstrap -->
    <!-- <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script> -->
    <!-- bootstrap-progressbar -->
    <script src="vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="vendors/iCheck/icheck.min.js"></script>
    <script src="js/moment/moment.min.js"></script>
    <script src="js/datepicker/daterangepicker.js"></script>
    <!-- Custom Theme Scripts -->
    <!-- <script src="vendors/switchery/dist/switchery.min.js"></script> -->
    <script src="js/custom.js"></script>
    <script src="js/include.js"></script>
    <!-- <script src="js/manage.js"></script> -->
    <script src="js/angularcustom.js"></script>