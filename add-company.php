        <?php 
        session_start();?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Create Company Details</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li>
                      <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br>
                  <form id="add_company" class="form-vertical form-label-left" ng-submit="addcompany()" name="addcompany">
                  <input type="hidden" class="form-control" id="id" value="<?=$_GET['id']?>">
                    <div class="row">
                      <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                          <label class="control-label">Company Name <span class="required">*</span></label>
                          <div>
                            <input type="text" required class="form-control" id="companyName" name="companyName" ng-model="cmp.companyName">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label">Phone</label>
                          <div>
                            <input type="text" class="form-control" id="Phone" name="Phone" ng-model="cmp.phone">
                          </div>
                        </div>                         
                        <div class="form-group">
                          <label class="control-label">Logo URL <span class="required">*</span></label>
                          <div>
                            <input type="text" class="form-control" id="logoURL" name="logoURL" required ng-model="cmp.logoURL">
                          </div>
                        </div>  
                        <div class="form-group">
                          <label class="control-label">Location <span class="required">*</span></label>
                          <div>
                            <input type="text" required class="form-control" id="location" name="location" ng-model="cmp.location">
                          </div>
                        </div>                      
                        <div class="form-group">
                          <label class="control-label">Address<span class="required">*</span></label>
                          <div>
                            <textarea rows="2" class="form-control" id="address" name="address" required ng-model="cmp.address"></textarea>
                          </div>
                        </div>                       
                      </div>
                      <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                          <label class="control-label">Website <span class="required">*</span></label>
                          <div>
                            <input type="text" required class="form-control" id="website" name="website" ng-model="cmp.website">
                          </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Size<span class="required">*</span></label>
                            <div>
                              <select required class="form-control" id="size" name="size" ng-model="cmp.size">
                              <?php foreach($_SESSION['constants']['COMPANY_SIZES'] as $key=>$val){ ?>
                                <option value="<?=$key?>"><?=$val?></option>
                              <?php }  ?>
                              </select>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label">Industry<span class="required">*</span></label>
                            <div>
                              <select required class="form-control" id="industry" name="industry" ng-model="cmp.industry">
                              <?php foreach($_SESSION['constants']['INDUSTRIES'] as $kind=>$vind){ ?>
                                <option value="<?=$kind?>"><?=$vind?></option>
                              <?php } ?>
                              </select>
                            </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label">About<span class="required">*</span></label>
                          <div>
                            <textarea rows="2" class="form-control" id="about" name="about" required ng-model="cmp.about"></textarea>
                          </div>
                        </div> 
                        <div class="form-group">
                          <label class="control-label">Verified</label>
                            <input type="checkbox" class="" id="verified" ng-model="cmp.verified">
                        </div>
                      </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-xs-12 text-right" id="remobtn">
                       <button type="reset" class="btn btn-primary" ui-sref="company">Cancel</button>
                        <button type="submit" class="btn btn-success">Submit</button>
                      </div>
                    </div>

                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->