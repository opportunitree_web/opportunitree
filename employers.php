        <!-- page content -->
        <div class="" role="main">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>All Employers List</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li>
                      <a ui-sref="addemployer">
                        <i class="fa fa-plus"></i>
                      </a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <div class="table-responsive">
                    <table class="table table-striped jambo_table bulk_action">
                      <thead>
                        <tr class="headings">
                          <th class="column-title" style="display: table-cell;">Name</th>
                          <th class="column-title" style="display: table-cell;">Email</th>
                          <th class="column-title" style="display: table-cell;">Profile Created</th> 
                          <th class="column-title" style="display: table-cell;">Enabled</th>
                          <th class="column-title" style="display: table-cell;">Activated</th>
                          <th class="column-title no-link last" style="display: table-cell;"><span class="nobr">Last Active</span>
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                    <tr ng-repeat="user in users" ui-sref="empdetails({id:user.id})">
                    <td>{{user.name}}</td>
                    <td>{{user.email}}</td>
                    <td>{{user.profileCreated}}</td>
                    <td>{{user.enabled}}</td>
                    <td>{{user.activated}}</td>
                    <td>{{user.lastActive}}</td>
                    </tr>
                    </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
        </div>
        <!-- /page content -->
      
