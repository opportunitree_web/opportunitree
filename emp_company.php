<div class="table-responsive">
  <table class="table table-striped jambo_table bulk_action">
    <thead>
      <tr class="headings">
        <th class="column-title" style="display: table-cell;">Sl. No. </th>
        <th class="column-title" style="display: table-cell;">Sender</th>
        <th class="column-title" style="display: table-cell;">Subject</th>
        <th class="column-title" style="display: table-cell;">Status </th>
        <th class="column-title no-link last" style="display: table-cell;"><span class="nobr">Action</span>
        </th>
      </tr>
    </thead>
    <tbody>
      <tr class="even pointer">
        <td class=" ">1</td>
        <td class=" ">John Blank L</td>
        <td>Lorem ipsum doler...</td>
        <td class=" ">Unread</td>
        <td class="last">
          <button class="btn btn-xs btn-primary"><i class="fa fa-eye"></i></button>
        </td>
      </tr>
      <tr class="odd pointer">
        <td class=" ">2</td>
        <td class=" ">John Blank L</td>
        <td>Lorem ipsum doler...</td>
        <td class=" ">Read</td>
        <td class=" last">
          <button class="btn btn-xs btn-primary"><i class="fa fa-eye"></i></button>
        </td>
      </tr>
    </tbody>
  </table>
</div>