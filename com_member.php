<div class="table-responsive">
  <table class="table table-striped jambo_table bulk_action">
    <thead>
      <tr class="headings">
        <th class="column-title" style="display: table-cell;">Member ID</th>
        <th class="cloumn-title" style="display: table-cell;">Action</th>
      </tr>
    </thead>
    <tbody>
      <tr ng-repeat="member in comMembers" class="even pointer">
        <td>{{member.id}}</td>
        <td>
          <button class="btn btn-xs btn-danger" title="Delete Member">
            <i class="fa fa-trash-o"></i>
          </button>
        </td>
      </tr>
    </tbody>
  </table>
</div>