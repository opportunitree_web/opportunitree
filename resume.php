
        <div class="" role="main">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>All Resumes List</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li>
                      <a ui-sref="addresume">
                        <i class="fa fa-plus"></i>
                      </a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <div class="table-responsive">
                    <table class="table table-striped jambo_table bulk_action">
                      <thead>
                        <tr class="headings">
                          <th class="column-title" style="display: table-cell;">Name</th>
                          <th class="column-title" style="display: table-cell;">Theme Id</th>
                          <th class="column-title" style="display: table-cell;">Public</th>
                          <th class="column-title" style="display: table-cell;">isPDFonly</th>
                          <th class="column-title no-link last" style="display: table-cell;"><span class="nobr">Views</span>
                          </th>
                        </tr>
                      </thead>
                      <tbody class='append'>
                        <tr ng-repeat="user in users" ui-sref="rsdetails({id:user.id})">
                    <td>{{user.firstName}} {{user.lastName}}</td>
                    <td>{{user.themeID}}</td>
                    <td>{{user.public}}</td>
                    <td>{{user.isPDFOnly}}</td>
                    <td>{{user.views}}</td>
                    </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- <div class="pull-right">
                  <ul class="pagination">
                  <li class="paginate_button previous disabled" id="datatable-keytable_previous">
                    <a href="#" aria-controls="datatable-keytable" data-dt-idx="0" tabindex="0">Previous</a>
                  </li>
                  <li class="paginate_button active">
                    <a href="#" aria-controls="datatable-keytable" data-dt-idx="1" tabindex="0">1</a>
                  </li>
                  <li class="paginate_button ">
                    <a href="#" aria-controls="datatable-keytable" data-dt-idx="2" tabindex="0">2</a>
                  </li>
                  <li class="paginate_button ">
                    <a href="#" aria-controls="datatable-keytable" data-dt-idx="3" tabindex="0">3</a>
                  </li>
                  <li class="paginate_button ">
                    <a href="#" aria-controls="datatable-keytable" data-dt-idx="4" tabindex="0">4</a>
                  </li>
                  <li class="paginate_button ">
                    <a href="#" aria-controls="datatable-keytable" data-dt-idx="5" tabindex="0">5</a>
                  </li>
                  <li class="paginate_button ">
                    <a href="#" aria-controls="datatable-keytable" data-dt-idx="6" tabindex="0">6</a>
                  </li>
                  <li class="paginate_button next" id="datatable-keytable_next">
                    <a href="#" aria-controls="datatable-keytable" data-dt-idx="7" tabindex="0">Next</a>
                  </li>
                </ul>
                </div> -->
              </div>
            </div>
        </div>
        