        <!-- page content -->
        <div class="" role="main">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Create A Resume</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li>
                      <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br>
                  <form id="add_resume"  class="form-vertical form-label-left" role="form" ng-submit="addresume()">
                    <div class="row">
                      <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                          <label class="control-label">Job Seeker ID <span class="required">*</span></label>
                          <div>
                            <input type="text" id="jobSeekerID" required class="form-control" name="jobSeekerID" ng-model="rs.jobSeekerID">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label" for="first-name">First Name <span class="required">*</span></label>
                          <div>
                            <input type="text" id="firstName" required class="form-control" name="firstName" ng-model="rs.firstName">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label" for="middle-name">Middle Name</label>
                          <div>
                            <input type="text" id="middleName" class="form-control" name="middleName" ng-model="rs.middleName">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label" for="last-name">last Name <span class="required">*</span></label>
                          <div>
                            <input type="text" id="lastName" required class="form-control" name="lastName" ng-model="rs.lastName">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label">Qualification</label>
                          <div>
                            <input type="text" id="qualifications" class="form-control" name="qualifications" ng-model="rs.qualifications">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label">Photo Url</label>
                          <div>
                            <input type="text" id="photoURL" class="form-control" name="photoURL" ng-model="rs.photoURL">
                          </div>
                        </div> 
                        <div class="form-group">
                          <label class="control-label">Video Url</label>
                          <div>
                            <input type="text" id="videoURL" class="form-control" name="videoURL" ng-model="rs.videoURL">
                          </div>
                        </div>                         
                        <div class="form-group">
                          <label class="control-label">Address<span class="required">*</span></label>
                          <div>
                            <textarea rows="3" class="form-control" id="address" name="address" ng-model="rs.address"></textarea>
                          </div>
                        </div>                                                              
                      </div>
                      <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                          <div class="row">
                            <br>
                            <div class="col-md-6 col-xs-12">
                              <div>
                                <label>
                                  Public <input type="checkbox" id="public" onclick="checkpub();" ng-model="rs.public"/>
                                </label>
                              </div>
                            </div>
                            <div class="col-md-6 col-xs-12">
                              <div>
                                <label>
                                   Is PDF <input type="checkbox" id="isPDFOnly" onclick="checkpdf();" ng-model="rs.isPDFOnly"/>
                                </label>
                              </div>
                            </div>
                          </div>
                        </div>
                          <div class="form-group" id="pdfhide" style="display:none;">
                            <label class="control-label">PDF Url<span class="required">*</span></label>
                            <div>
                              <input type="text" class="form-control" id="pdfURL" name="pdfURL" ng-model="rs.pdfURL">
                            </div>
                        </div>
                          <div class="form-group">
                            <label class="control-label">Theme Id<span class="required">*</span></label>
                            <div>
                              <input type="text" required class="form-control" id="themeID" name="themeID" ng-model="rs.themeID">
                            </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label">Work Preference <span class="required" >*</span>
                          </label>
                          <p style="padding: 5px;">
                            <input type="checkbox" id="wp1" /> Morning
                            <input type="checkbox" id="wp2" /> Afternoon
                            <input type="checkbox" id="wp3" /> Evening
                            <input type="checkbox" id="wp4" /> Overnight
                            <br><br>
                            <input type="checkbox" id="wp5" /> Weekdays
                            <input type="checkbox" id="wp6" /> Weekends
                          <p>
                        </div>
                        <div class="form-group">
                          <label class="control-label">Hours PerWeek <span class="required">*</span></label>
                          <div>
                            <input type="text" class="form-control" required id="hoursPerWeek" name="hoursPerWeek" ng-model="rs.hoursPerWeek">
                          </div>
                        </div>  
                        <div class="form-group">
                          <label class="control-label"> Availability</label>
                          <div>
                              <input type="text" id="availability" class="form-control" name="availability" ng-model="rs.availability"/>                   
                          </div>
                        </div>  
                        <!-- <div class="form-group">
                          <label class="control-label">Views</label>
                          <div>
                            <input type="text" id="videothumbnail" required class="form-control" name="videothumbnail">
                          </div>
                        </div> -->                       
                        <div class="form-group">
                          <label class="control-label"> Start Working</label>
                          <div>
                            <input type="date" id="startWorking" class="dateall form-control" name="startWorking" ng-model="rs.startWorking"/>
                          </div>
                        </div>                     
                        <div class="form-group">
                          <label class="control-label">Video Thumbnail</label>
                          <div>
                            <input type="text" id="videoThumbnail" class="form-control" name="videoThumbnail" ng-model="rs.videoThumbnail">
                          </div>
                        </div>             
                      </div>
                      <div class="col-sm-12">
                        <div class="row">
                          <div class="col-sm-12"><b><i>WORK EXPERIENCE</i></b></div>
                          <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                              <label for="">Title</label>
                              <div><input type="text" class="form-control" name="workExp['title']" id="workexp_title" ng-model="rs.workExptitle"></div>
                            </div>
                          </div>
                          <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                              <label for="">Time Start</label>
                              <div><input type="date" class="form-control dateall" name="workExp['timespanStart']" id="workexp_timespanStart" ng-model="rs.workExptimespanStart"></div>
                            </div>
                          </div>
                          </div>
                          <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                              <label for="">Time End</label>
                              <div><input type="date" class="form-control dateall" name="workExp['timespanEnd']" id="workexp_timespanEnd" ng-model="rs.workExptimespanEnd"></div>
                            </div>
                          </div>
                          <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                              <label for="">Company</label>
                              <div><input type="text" class="form-control" name="workExp['company']" id="workexp_company" ng-model="rs.workExpcompany"></div>
                            </div>
                          </div>
                          <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                              <label for="">Location</label>
                              <div><input type="text" class="form-control" name="workExp['location']" id="workexp_location" ng-model="rs.workExplocation"></div>
                            </div>
                          </div>
                          <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                              <label for="">Task</label>
                              <div><input type="text" class="form-control" name="workExp['tasks']" id="workexp_tasks" ng-model="rs.workExptasks"></div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="row">
                          <div class="col-sm-12"><b><i>EDUCATION DETAILS</i></b></div>
                          <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                              <label for="">Title</label>
                              <div><input type="text" class="form-control" name="education['title']" id="edu_title" ng-model="rs.educationtitle"></div>
                            </div>
                          </div>
                          <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                              <label for="">Time Start</label>
                              <div><input type="date" class="form-control dateall" name="education['timespanStart']" id="edu_timespanStart" ng-model="rs.educationtimespanStart"></div>
                            </div>
                          </div>
                          <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                              <label for="">Time End</label>
                              <div><input type="date" class="form-control dateall" name="education['timespanEnd']" id="edu_timespanEnd" ng-model="rs.educationtimespanEnd"></div>
                            </div>
                          </div>
                          <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                              <label for="">Institute</label>
                              <div><input type="text" class="form-control"  name="education['institution']" id="edu_institution" ng-model="rs.educationinstitution"></div>
                            </div>
                          </div>
                          <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                              <label for="">Location</label>
                              <div><input type="text" class="form-control" name="education['location']" id="edu_location" ng-model="rs.educationlocation"></div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="row">
                          <div class="col-sm-12"><b><i>VOLUNTARY EXPERIENCE</i></b></div>
                          <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                              <label for="">Title</label>
                              <div><input type="text" class="form-control" name="volunteerExp['title']" id="volun_title" ng-model="rs.volunteerExptitle"></div>
                            </div>
                          </div>
                          <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                              <label for="">Time Start</label>
                              <div><input type="date" class="form-control dateall" name="volunteerExp['timespanStart']" id="volun_timespanStart" ng-model="rs.volunteerExptimespanStart"></div>
                            </div>
                          </div>
                          <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                              <label for="">Time End</label>
                              <div><input type="date" class="form-control dateall" name="volunteerExp['timespanEnd']" id="volun_timespanEnd" ng-model="rs.volunteerExptimespanEnd"></div>
                            </div>
                          </div>
                          <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                              <label for="">Organisation</label>
                              <div><input type="text" class="form-control" name="volunteerExp['organization']" id="volun_organization" ng-model="rs.volunteerExporganization"></div>
                            </div>
                          </div>
                          <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                              <label for="">Location</label>
                              <div><input type="text" class="form-control" name="volunteerExp['location']" id="volun_location" ng-model="rs.volunteerExplocation"></div>
                            </div>
                          </div>
                          <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                              <label for="">Task</label>
                              <div><input type="text" class="form-control" name="volunteerExp['tasks']" id="volun_tasks" ng-model="rs.volunteerExptasks"></div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-xs-12 text-right">
                        <div class="col-xs-12 text-right" id="remobtn">
                        <button ui-sref="resume" class="btn btn-primary">Cancel</button>
                        <button type="submit" class="btn btn-success" ng-hide="rs.id">Submit</button>
                        <button type="submit" class="btn btn-success" ng-hide="!rs.id">Update</button>
                      </div>
                      </div>
                    </div>

                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
