        <!-- page content -->
        <div class="" role="main">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Create A Job</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li>
                      <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br>
                  <form id="add_job" name="addformjob" class="form-vertical form-label-left" role="form" ng-submit="addjob()">
                    <div class="row">
                      <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                          <label class="control-label">Company ID <span class="required">*</span></label>
                          <div>
                            <input type="text" id="companyID" required class="form-control" name="companyID" ng-model="job.companyID">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label" >Title <span class="required">*</span></label>
                          <div>
                            <input type="text" id="title" required class="form-control" name="title" ng-model="job.title">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label">Description<span class="required">*</span></label>
                          <div>
                            <textarea name="description" class="form-control" required id="description" ng-model="job.description"></textarea>
                          </div>
                        </div>                        
                        <div class="form-group">
                          <label class="control-label">Address<span class="required">*</span></label>
                          <div>
                            <textarea rows="2" class="form-control" required name="address" id="address" ng-model="job.address"></textarea>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label">Max Applicants</label>
                          <div>
                            <input type="text" class="form-control" name="maxApplicants" id="maxApplicants" ng-model="job.maxApplicants">
                          </div>
                        </div>                       
                      </div>
                      <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <label class="control-label">Availability<span class="required">*</span></label>
                            <div>
                              <input type="text" required class="form-control" name="availability" id="availability" ng-model="job.availability">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label">Job Type<span class="required">*</span></label>
                            <div>
                              <input type="text" required class="form-control" id="jobType" name="jobType" ng-model="job.jobType">
                            </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label">Wage Class <span class="required">*</span>
                          </label>
                          <div>
                            <input class="form-control" required type="text" id="wageClass" name="wageClass" ng-model="job.wageClass">
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="row">
                            <br>
                            <div class="col-md-4 col-xs-12">
                              <div>
                                <label>
                                  Apply On OT <input type="checkbox" id="applyOnOT" ng-model="job.applyOnOT" onclick="addexternalurl();" />
                                </label>
                              </div>
                            </div>
                            <div class="col-md-4 col-xs-12">
                              <div>
                                <label>
                                  Featured <input type="checkbox" id="featured" ng-model="job.featured"/>
                                </label>
                              </div>
                            </div>
                            <div class="col-md-4 col-xs-12">
                              <div>
                                <label>
                                  Open <input type="checkbox" id="open" checked ng-model="job.open"/>
                                </label>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label">External Url:
                          </label>
                          <div>
                            <input class="form-control" id="applyExternURL" type="text" name="applyExternURL" ng-model="job.applyExternURL" required>
                          </div>
                        </div>

                      </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-xs-12 text-right">
                      <a type="button" ui-sref="jobs" class="btn btn-primary">Cancel</a>
                        <button type="submit" class="btn btn-success" ng-hide="job.id">Submit</button>
                        <button type="submit" class="btn btn-success" ng-hide="!job.id">Update</button>
                      </div>
                    </div>

                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
