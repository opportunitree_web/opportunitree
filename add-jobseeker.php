        <!-- page content -->
        <div class="right_col" role="main">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Create A Job Seeker</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li>
                      <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br>
                  <form id="jobseekerform" name="jobseekerform" class="form-vertical form-label-left" role="form" ng-submit="addjobseeker()">
                    <div class="row">
                      <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                          <label class="control-label" for="first-name">Name <span class="required">*</span></label>
                          <div>
                            <input type="text" id="name" required class="form-control" ng-model="js.name" name="name">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label">Email<span class="required">*</span></label>
                          <div>
                            <input type="text" required class="form-control" id='email' ng-model="js.email" name="email">
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="row">
                            <br>
                            <div class="col-md-6 col-xs-12">
                              <div>
                                <label>
                                  Enabled <input type="checkbox" id="enabled" ng-model="js.enabled"/>
                                </label>
                              </div>
                            </div>
                            <div class="col-md-6 col-xs-12">
                              <div>
                                <label>
                                  Activated <input type="checkbox" id="activated" ng-model="js.activated"/>
                                </label>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label">Phone <span class="required">*</span>
                          </label>
                          <div>
                            <input class="form-control" type="text" ng-model="js.phone" id="phone" name="phone" required>
                          </div>
                        </div>
                        <div class="form-group" id="rmpwd">
                          <label class="control-label">Password </label>
                          <div>
                            <input type="password" minlength="6" class="form-control" ng-model="js.password" id="password" required name="password">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                          <label class="control-label">Social IDs </label>
                          <div>
                            <input type="text" class="form-control" id='Socialid' ng-model="js.socialIDs" name="socialIDs" required>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label">Profile Created </label>
                          <div>
                            <input type="text" class="form-control dateall" ng-model="js.profileCreate" id="profileCreate" name="profileCreate">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label">Plan ID </label>
                          <div>
                            <input type="text" class="form-control" ng-model="js.planID" id="planID" name="planID">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label">Plan Expiration </label>
                          <div>
                            <input type="text" class="form-control dateall" ng-model="js.planExpiration" id="planExpiration" name="planExpiration" required>
                          </div>
                        </div>
                        
                      </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-xs-12 text-right" id="remobtn">
                        <a type="button" ui-sref="jobseeker" class="btn btn-primary">Cancel</a>
                        <button type="submit" class="btn btn-success" ng-hide="js.id">Submit</button>
                        <button type="submit" class="btn btn-success" ng-hide="!js.id">Update</button>
                      </div>
                    </div>

                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
