        <!-- page content -->
        <div class="" role="main">
          <div class="page-title">
            <div class="title_left">
              <h3 class="name"></h3>
            </div>
            <div class="title_right">
              <div class="col-xs-12 form-group text-right">
                <a class="btn btn-xs btn-success" ui-sref="editjobseeker({id:id})"><i class="fa fa-edit"></i></a>
                <button class="btn btn-xs btn-danger" ng-click=deleterecord(id,'job-seekers') ><i class="fa fa-trash-o"></i></button>
              </div>
            </div>
          </div>
          <div class="cleatfix"></div>
          <div class="row">
            <div class="col-md-7 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Users Info</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li>
                      <a ui-sref="editjobseeker({id:id})">
                        <i class="fa fa-pencil"></i>
                      </a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <p>
                    <b>Name:</b> <span class="name" ng-bind="name"></span>
                  </p>
                  <p>
                    <b>Email:</b> <span id="email" ng-bind="email"></span>
                  </p>
                  <p>
                    <b>Phone:</b> <span id="phone" ng-bind="phone"></span>
                  </p>
                  <p>
                    <b>Profile Created:</b> <span id="profilecreate" ng-bind="profilecreate"></span>
                  </p>
                  <p>
                    <b>Enabled:</b> <span id="enabled" ng-bind="enabled"></span>
                  </p>
                  <p>
                    <b>Activated:</b> <span id="activated" ng-bind="activated"></span>
                  </p>
                </div>
              </div>
              <div class="x_panel">
                <!-- INbox Thread -->
                <div ui-view="inboxview"></div>
              </div>
            </div>
            <div class="col-md-5 col-sm-12 col-xs-12">
              <div class="x_panel">
              <div ui-view="resumeview"></div>
              </div>
              <div class="x_panel">
              <div ui-view="jbappview"></div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
