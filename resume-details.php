        <!-- page content -->
        <div class="" role="main">
          <div class="page-title">
            <div class="title_left">
              <h3>{{name}} Resume</h3>
            </div>
            <div class="title_right">
              <div class="col-xs-12 form-group text-right">
                <button class="btn btn-xs btn-success" ui-sref="editresume({id:id})"><i class="fa fa-edit"></i></button>
                <button class="btn btn-xs btn-danger" ng-click="deleterecord(id,'resumes')"><i class="fa fa-trash-o"></i></button>
              </div>
            </div>
          </div>
          <div class="cleatfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Resume details</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li>
                      <a ui-sref="editresume({id:id})">
                        <i class="fa fa-pencil"></i>
                      </a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <p>
                    <b>Name:</b> <span id="name" ng-bind="name"></span>
                  </p>
                  <p>
                    <b>Photo Url:</b> <span id="photourl" ng-bind="photourl"></span>
                  </p>
                  <p>
                    <b>PDF URL:</b> <span id="pdfurl" ng-bind="pdfurl"></span>
                  </p>
                  <p>
                    <b>Address:</b> <span id="address" ng-bind="address"></span>
                  </p>
                  <p>
                    <b>Qualification:</b> <span id="qualification" ng-bind="qualification"></span>
                  </p>
                  <p>
                    <b>HoursPerWeek:</b> <span id="hoursPerWeek" ng-bind="hoursPerWeek"></span>
                  </p>
                  <p>
                    <b>ThemeID:</b> <span id="themeid" ng-bind="themeid"></span>
                  </p>
                  <p>
                    <b>StartWorking:</b> <span id="startWorking" ng-bind="startWorking"></span>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
