        <!-- page content -->
        <div class="" role="main">
          <div class="page-title">
            <div class="title_left">
              <h3>{{title}}</h3>
            </div>
            <div class="title_right">
              <div class="col-xs-12 form-group text-right">
                <button class="btn btn-xs btn-success" ui-sref="editjob({id:id})"><i class="fa fa-edit"></i></button>
                <button class="btn btn-xs btn-danger" ng-click="deleterecord(id)"><i class="fa fa-trash-o"></i></button>
              </div>
            </div>
          </div>
          <div class="cleatfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Job Info</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li>
                      <a ui-sref="editjob({id:id})">
                        <i class="fa fa-pencil"></i>
                      </a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <p>
                    <b>Company Name:</b> {{companyName}}
                  </p>
                  <span>
                    <b>Company Logo:</b>
                    <img src="{{companyLogoURL}}" alt="Logo" style="height: 75px;">
                  </span>                  
                  <p>
                    <b>Address:</b> {{address}}
                  </p>
                  <p>
                    <b>Apply or Not:</b> {{applyOnOT}}
                  </p>
                  <p>
                    <b>Feature:</b> {{featured}}
                  </p>
                  <p>
                    <b>Open:</b> {{open}}
                  </p>
                   <p>
                    <b>Description:</b> {{description}}
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
