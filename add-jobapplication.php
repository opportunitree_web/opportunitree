        <!-- page content -->
        <div class="" role="main">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Create A Job Application</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li>
                      <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br>
                  <form id="add_jobapplication" class="form-vertical form-label-left" name="jobapplication" ng-submit="addapplicaion();">
                    <div class="row">
                      <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                          <label class="control-label">Job ID <span class="required">*</span></label>
                          <div>
                            <input type="text" required="required" class="form-control" id="jobID" name="jobID" ng-model="japp.jobID">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label">Resume ID<span class="required">*</span></label>
                          <div>
                            <input type="text" required="required" class="form-control" id="resumeID" name="resumeID" ng-model="japp.resumeID">
                          </div>
                        </div>                        
                        <div class="form-group">
                          <label class="control-label">Job Seeker ID<span class="required">*</span></label>
                          <div>
                            <input type="text" required="required" class="form-control" id="jobSeekerID" name="jobSeekerID" ng-model="japp.jobSeekerID">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label">Company ID</label>
                          <div>
                            <input type="text" class="form-control" id="companyID" name="companyID" ng-model="japp.companyID">
                          </div>
                        </div>                         
                      </div>
                      <div class="col-md-6 col-sm-12">
                          <div class="form-group">
                            <label class="control-label">Cover Letter<span class="required">*</span></label>
                            <div>
                              <textarea type="text" required="required" class="form-control" id="coverLetter" name="coverLetter" ng-model="japp.coverLetter"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label">Custom Resume URL</label>
                          <div>
                            <input type="text" class="form-control" id="customResumeURL" name="customResumeURL" ng-model="japp.customResumeURL">
                          </div>
                        </div>  
                        <div class="form-group">
                          <div>
                                <label>
                                  Refer ListAccess <input type="checkbox" id="refListAccess" ng-model="japp.refListAccess"/>
                                </label>
                              </div>
                        </div>
                      </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-xs-12 text-right"  id="remobtn">
                        <button type="reset" class="btn btn-primary">Cancel</button>
                        <button type="submit" class="btn btn-success">Submit</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
