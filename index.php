<!DOCTYPE html>
<html>
<head>
    <?php include_once 'common/head.php';?>
    <?php include_once 'common/footer.php';?>
</head>
<body ng-app="oppertunitreeApp" class="nav-md">
<div class="container body">
      <div class="main_container">
        <?php include_once 'common/menu.php';?>
        <div class="right_col" role="main">
           <div ui-view></div>
        </div>
     </div>
</div>
</body>
</html>
