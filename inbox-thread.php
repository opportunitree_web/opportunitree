<div class="x_title">
                  <h2>Inbox Threads</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li>
                      <a ui-sref="addinbox">
                        <i class="fa fa-plus"></i>
                      </a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <div class="table-responsive">
                    <table class="table table-striped jambo_table bulk_action">
                      <thead>
                        <tr class="headings">
                          <th class="column-title" style="display: table-cell;">Sender</th>
                          <th class="column-title" style="display: table-cell;">Subject</th>
                          <th class="column-title" style="display: table-cell;">Status </th>
                          </th>
                        </tr>
                      </thead>
                      <tbody ng-repeat="user in users">
                        <tr class="even pointer">
                          <td class=" ">{{user.id}}</td>
                          <td>{{user.message}}</td>
                          <td class=" ">{{user.employerSent}}</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>