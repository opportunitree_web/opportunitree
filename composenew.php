        <!-- page content -->
        <div role="main">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Compose A New Message</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <div class="row">
                    <div class="col-md-6 col-md-offset-3 col-sm-12">
                      <div class="form-group">
                          <label class="control-label">Job Seeker ID <span class="required">*</span></label>
                          <div>
                            <input type="text" id="name" required class="form-control">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label">Company ID <span class="required">*</span></label>
                          <div>
                            <input type="text" id="name" required class="form-control">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label">Job Application ID <span class="required">*</span></label>
                          <div>
                            <input type="text" id="name" required class="form-control">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label">Subject <span class="required">*</span></label>
                          <div>
                            <textarea rows="3" class="form-control"></textarea>
                          </div>
                        </div>
                    </div>
                  </div>
                  <br>
                  <form class="form-vertical form-label-left" role="form">
                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-xs-12 text-right">
                        <a type="button" class="btn btn-primary">Cancel</a>
                        <button type="submit" class="btn btn-success">Submit</button>
                      </div>
                    </div>

                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
