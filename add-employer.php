        <!-- page content -->
        <div class="right_col" role="main">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Create An Employer</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li>
                      <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br>
                  <form id="add_employee" class="form-vertical form-label-left" ng-submit="addemployee()">
                    <input type="hidden" class="form-control" id="id" value="<?=$_GET['id']?>">
                    <div class="row">
                      <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                          <label class="control-label" for="first-name">Name <span class="required">*</span></label>
                          <div>
                            <input type="text" id="emp_name" name="name" ng-model="emp.name" required="required" class="form-control">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label">Email<span class="required">*</span></label>
                          <div>
                            <input type="text" id="emp_email" required="required" name="email" ng-model="emp.email" class="form-control">
                          </div>
                        </div>
                        <div class="form-group" id="emp_passwordForm">
                          <label class="control-label">Password<span class="required">*</span></label>
                          <div>
                            <input type="password" id="emp_password" required="required" ng-model="emp.password" name="password" class="form-control">
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="row">
                            <br>
                            <div class="col-md-6 col-xs-12">
                              <div>
                                <label>
                                  Enabled <input type="checkbox" class="js-switch" id="emp_enabled" checked ng-model="emp.enabled"/>
                                </label>
                              </div>
                            </div>
                            <div class="col-md-6 col-xs-12">
                              <div>
                                <label>
                                  Activated <input type="checkbox" class="js-switch" id="emp_activated" checked ng-model="emp.activated"/>
                                </label>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label">Phone <span class="required">*</span>
                          </label>
                          <div>
                            <input class="form-control" name="emp_phone" required="required" type="text" ng-model="emp.emp_phone">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                          <label class="control-label">Social IDs </label>
                          <div>
                            <input type="text" name="emp_social" class="form-control" ng-model="emp.emp_social">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label">Profile Created </label>
                          <div>
                            <input type="text" id="emp_profileCrt" name="emp_profDate" class="form-control" ng-model="emp.emp_profileCrt">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label">Plan ID </label>
                          <div>
                            <input type="text" id="emp_plan" name="emp_planid" class="form-control" ng-model="emp.emp_planid">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label">Plan Expiration </label>
                          <div>
                            <input type="text" name="emp_planexp" id="emp_planExp" class="form-control dateall" ng-model="emp.emp_planExp">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-xs-12 text-right" id="remobtn">
                        <button ui-sref="employers" class="btn btn-primary">Cancel</button>
                        <button type="submit" class="btn btn-success" >Submit</button>
                      </div>
                    </div>

                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->